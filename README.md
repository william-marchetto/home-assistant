# Setup

* Edit `_copy_configs.sh` to include the relevant paths to HA installation.
* Use setup, start, and restart scripts to run Home Assistant.
* Make all config file edits in this repo - these overwrite the copies in the HA install directory.

# Auto-Off Configuration

The entry lights are setup to automatically turn off after a set amount of time (through HA rather than the Kasa app). This is done so that auto-off can be disabled when setting a scene. A scene can disable the Lights Auto-Off boolean helper to stop the auto-off feature. This should be re-enabled when deactivating the scene (e.g. by calling the Goodnight scene).

# Update

* docker pull <image:latest> (mqtt and hass)
* docker stop <id>; docker rm <id>
* ./setup_hass.sh
