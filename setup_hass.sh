./_copy_configs.sh

docker run -d --name homeassistant \
    --privileged \
    --restart=unless-stopped \
    -e TZ=America/Chicago \
    -v /home/pi/hass:/config \
    --device /dev/ttyAMA0:/dev/ttyAMA0 \
    --network=host \
    ghcr.io/home-assistant/home-assistant:stable

docker run -dit -p 8091:8091 -p 3000:3000 --restart=unless-stopped --device=/dev/serial/by-id/usb-0658_0200-if00:/dev/zwave --mount source=zwavejs2mqtt,target=/usr/src/app/store zwavejs/zwavejs2mqtt:latest
