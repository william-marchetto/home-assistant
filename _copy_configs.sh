# Updates HA config files with latest from repo
cp config/* ~/hass/
cp blueprints/* ~/hass/blueprints/automation/wrmarchetto

# Backup automations and scene files
cp ~/hass/automations.yaml backup
cp ~/hass/scenes.yaml backup
